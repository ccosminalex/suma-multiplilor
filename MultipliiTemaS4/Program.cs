﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipliiTemaS4
{
    
    class Program
    {
        static int SumaMul(int a, int b)
        {
            int suma = a;
            while (suma < b)
                suma = suma + a;
            return suma - a;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Dati primul nr: ");
            int nr1 = int.Parse(Console.ReadLine(), out nr1);
            Console.WriteLine("Dati al 2 lea nr: ");
            int nr2 = int.Parse(Console.ReadLine(), out nr2);
            SumaMul(nr1, nr2);
            Console.ReadKey();
        }
    }
}
